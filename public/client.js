function GameClient() {
	this.ws = new WebSocket("ws://" + window.location.host + "/socket");
	this.connected = false;
	this.onDirection = function(direction, gameId) {
		switch (direction) {
			case "up": 
				games[gameId].racket.moveUp();
				break;
			case  "down":
				games[gameId].racket.moveDown();
				break;
		}
	}

	c = this;
	this.ws.onopen = function() {
		console.log("Connection established")
		c.connected = true;
		loop();
	}

	this.ws.onmessage = function(event) {
	  var m = JSON.parse(event.data);

	  switch(m.type) {
	  	case "direction":
	  		c.onDirection(m.data.direction, m.data.gameId);
	  	break;

	  	case "reset":
	  		console.log("Restarting!");
	  		reset();
	  }
	}

	this.ws.onerror = function(event) {
	  console.log('Error:' + event)
	  noLoop();
    }
    
    this.send = function(game) {
		gameState = {
			gameId: game.id,
			gameState: game.state,
			racket: {
				top: game.racket.top,
				bottom: game.racket.top
			},
			ball: {
				x: int(game.ball.x),
				y: int(game.ball.y),
			},
			racket: {
				top: int(game.racket.top),
				bottom: int(game.racket.bottom),
			},
			score: game.score,
		};
		if (this.connected){
			this.ws.send(JSON.stringify(gameState));
		}
    }
}
