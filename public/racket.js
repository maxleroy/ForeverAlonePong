let RacketStartX = canvas.x - racketWidth - 5;

function Racket(rgb) {
    this.rgb = rgb;
    this.x = canvas.x - racketWidth - 5;
    this.y = canvas.y/2 - racketHeight/2;
    
    this.yspeed = 0;

    this.height = racketHeight;
    this.width = racketWidth;

    this.updateTopBottom = function() {
        this.top = this.y;
        this.bottom = this.y + this.height;
    }

    this.update = function (){
        this.updateTopBottom();
        this.display();
    }

    this.display = function() {
        noStroke()
        fill(this.rgb[0],this.rgb[1],this.rgb[2]);
        rect(this.x, this.y, this.width, this.height);
    }

    this.moveUp = function () {
        if (this.top - 6 >= 0) {
            this.y -= 1;
        }
    }

    this.moveDown = function () {
        if (this.bottom + 6 <= canvas.y) {
            this.y += 1;
        }
    }
}