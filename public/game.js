function Game(id) {
    this.red = random(0,255)
    this.green = random(0,255)
    this.blue = random(0,255)
    this.id = id
    this.state = 'ready';
    this.score = 0;
    this.ball = new Ball(425,300,[this.red,this.green,this.blue]);
    this.racket = new Racket([this.red,this.green,this.blue]);
    
    this.update = function() {
        this.ball.update();
        this.racket.update();
        this.checkWinLooseCondition();
    }

    this.checkWinLooseCondition = function() {
        if (this.ball.x + this.ball.radius*2 >= canvas.x + 7) {
            if ((this.ball.y > this.racket.top) && (this.ball.y < this.racket.bottom)) {
                this.score++;
                if (this.score > bestScore) {
                    bestScore = this.score;
                }
                if (this.score > bestScoreEver) {
                    bestScoreEver = this.score;
                }
                if (this.score % 3 == 0) {
                    this.ball.velocity++;
                }
                this.ball.bounce(this.racket.top);
                return;
            }
        }

        if (this.ball.x + this.ball.radius >= canvas.x + 5) {
            games[this.id].state = 'over';
        }
    }
}