let canvas= {x:640, y:480};
let fps = 30;
let racketWidth = 10;
let racketHeight = 100;
let bestScore = 0;
let bestScoreEver = 0;
let generation = 0;
let games = new Array;
let numberOfGames = 10;

function setup() {
  createCanvas(canvas.x,canvas.y);
  background(0);
  frameRate(fps);

  for(i=0;i<numberOfGames;i++) {
    let game = new Game(i);
    games.push(game)
  }

  client = new GameClient();
}

function draw() {
  background(0);
  drawWalls();
  drawData();
  
  for (gameId=0;gameId<numberOfGames;gameId++) {
    //if (frameCount % 10 == 0){
      client.send(games[gameId]);
    //}
    if (games[gameId].state != 'over') {
      games[gameId].update();
    }
  }
}

function drawWalls() {
  stroke("red");
  strokeWeight(12);
  line(0, 0, 0, canvas.y);
  line(0, 0, canvas.x, 0);
  line(0, canvas.y, canvas.x, canvas.y);
}

function drawData() {
  for(i=0;i<numberOfGames;i++) {
    textSize(12);
    noStroke();
    fill(255);
    text("Best score " + bestScore , 23, 30);
    text("Best score Ever " + bestScoreEver , 23, 60);
    text("Generation " + generation , 23, 90);
  }
}

function reset() {
  games = new Array();
  for(i=0;i<numberOfGames;i++) {
    let game = new Game(i);
    games.push(game);
  }
  generation++;
  bestScore = 0;
  loop();
}

