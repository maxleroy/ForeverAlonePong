function Ball(x,y,rgb) {
    this.radius = 15;
    this.rgb = rgb;
    this.x = -this.radius*2 + canvas.x + 9;
    this.y = canvas.y/2 -this.radius;
    
    this.xspeed = int(random(-18,-15));
    this.yspeed = int(random(-3,3));

    this.velocity = 15;

    this.display = function() {
        noStroke();
        fill(this.rgb[0],this.rgb[1],this.rgb[2]);
        ellipse(this.x, this.y, this.radius);
    }

    this.update = function() {
        this.collideWithWalls()
        this.x += this.xspeed;
        this.y += this.yspeed;
        this.display();
    }

    this.collideWithWalls = function() {
        if (this.x - this.radius <= 0) {
            this.bounceX();
        }

        if ((this.y - this.radius <= 0) || (this.y + this.radius >= canvas.y)) {
            this.bounceY();
        }
    }

    this.bounce = function(racketTop) {
        value = (this.y - racketTop) / racketHeight;
        angle = map(value,0,1, -3.14/3, 3.14/3);
        this.xspeed = -this.velocity * cos(angle) - int(random(-2,0));
        this.yspeed = this.velocity * sin(angle) - int(random(-2,0));
    }

    this.bounceX = function() {
        this.xspeed = -this.xspeed;
    }

    this.bounceY = function() {
        this.yspeed = -this.yspeed;
    }
}