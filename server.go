package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"golang.org/x/net/websocket"
)

const NUMBER_OF_CONCURRENT_GAMES = 10

func indexHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	view.ExecuteTemplate(w, "index.html", nil)
}

type RequestMessage struct {
	GameId int          `json:"gameId"`
	GameState string    `json:"gameState"`
	Score    int        `json:"score"`
	Ball     BallSubMessage       `json:"ball"`
	Racket   RacketSubMessage     `json:"racket"`
}

type BallSubMessage struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type RacketSubMessage struct {
	Top int    `json:"top"`
	Bottom int `json:"bottom"`
}

type ResponseMessage struct {
	Type string      `json:"type"`
	Data interface{} `json:"data"`
}

type DirectionMessage struct {
	Direction string `json:"direction"`
	GameId int `json:"gameId"`
}

type ResetMessage struct {
	Reset bool `json:"reset"`
}

//Add game reset response message

func socket(ws *websocket.Conn) {
	for {
		var m RequestMessage
		if err := websocket.JSON.Receive(ws, &m); err != nil {
			log.Println(err)
			break
		}
		
		Games[m.GameId].update(m)

		//log.Printf("Received message: %+v\n", m)
		//Thats basically my game loop on server side...
		var generationStillRunning bool = false
		for gameId:=0; gameId < NUMBER_OF_CONCURRENT_GAMES; gameId++ {
			if Games[gameId].State != "over" {	
				generationStillRunning = true
				r := ResponseMessage {
					Type: "direction",
					Data: DirectionMessage{
						Direction: Games[gameId].getDirection(),
						GameId: gameId,
					},
				}

				if err := websocket.JSON.Send(ws, r); err != nil {
					log.Println(err)
					break
				}
			}
		}

		if !generationStillRunning {
			createNewGeneration(Games)

			r := ResponseMessage {
				Type: "reset",
				Data: ResetMessage{
					Reset: true,
				},
			}
		
			if err := websocket.JSON.Send(ws, r); err != nil {
				log.Println(err)
				break
			}
		}
	}
}

func createNewGeneration(Games []Game) []Game {
	bestGames := determineBestGames(Games)
	for gameId:=0;gameId<NUMBER_OF_CONCURRENT_GAMES;gameId++ {
		var newBrain Network
		newBrain = bestGames[0].Brain
		
		//n := bestGames[g].Brain.CrossBreed(bestGames[g+1].Brain)
		if (gameId % 2 == 0) {
			newBrain.Mutate()
		}
		Games[gameId] = Game{Id:gameId,State:"ready",Brain:newBrain}
	}

	return Games
}


func determineBestGames(Games []Game) []Game {
	var bestGames []Game
	for g:=0; g<4; g++ {
		bestIndex := 0 
		for i, _ := range Games {
			if (Games[i].Score > Games[bestIndex].Score) {
				bestIndex = i
			}
		}
		bestGames = append(bestGames, Games[bestIndex])

		Games = append(Games[:bestIndex], Games[bestIndex+1:]...)
	}

	return bestGames
}

var view *template.Template
var Games []Game

func main() {
	fs := http.FileServer(http.Dir("public"))
	http.Handle("/public/", http.StripPrefix("/public/", fs))

	view = template.Must(template.ParseFiles("templates/index.html"))

	http.HandleFunc("/", indexHandler)
	http.Handle("/socket", websocket.Handler(socket))

	Games = make([]Game,NUMBER_OF_CONCURRENT_GAMES)
	for gameId:=0;gameId < NUMBER_OF_CONCURRENT_GAMES;gameId++ {
		var n Network;
		n.RandomInit(3,2,1,1);
		Games[gameId] = Game{Id:gameId,State:"ready",Brain:n}
	}

	fmt.Println("Server listening on port 8080")
	http.ListenAndServe(":8080", nil)
}
