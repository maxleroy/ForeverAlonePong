package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"math/rand"
	"time" 
)

const NEURAL_DEBUG = false

type Neuron struct {
	Weights []float64 
	Value float64
	Bias float64
	BiasWeight float64
	ActivationFunction activationFunction
	ActivationFunctionDerivative activationFunction
}

type Layer struct {
	Neurons []Neuron
	Size int
}

type Network struct {
	Layers []Layer
	LearningRate float64
	Size int
}

//----------------Neuron-----------------//
func (n *Neuron) init(numberOfInputs int) {
	randSource := rand.NewSource(time.Now().UnixNano())
	randGen := rand.New(randSource)

	n.Value = 0
	n.ActivationFunction = Relu
	n.ActivationFunctionDerivative = SigmoidalTransferDerivate
	n.Bias = 1
	n.BiasWeight = randGen.NormFloat64()
	n.Weights = make([]float64, numberOfInputs)
	for i, _ := range n.Weights {
		n.Weights[i] = randGen.NormFloat64()
	}
	//n.Weights = []float64{1,-1}
	if NEURAL_DEBUG {
		log.WithFields(log.Fields{
			"value":n.Value,
			"Weights":n.Weights,
		}).Info("Neuron initiated")
	}
}

//----------------Network-----------------//
func (n *Network) RandomInit(size, inputNeurons, neuronsPerHiddenLayers, outputNeurons int) {
	n.Size = size
	n.Layers = make([]Layer, size)
	var neuronsCount, previousNeuronCount int
	for i, _ := range n.Layers {
		if i == 0 {
			neuronsCount = inputNeurons
			previousNeuronCount = 0
		} else{
			previousNeuronCount = n.Layers[i-1].Size
			if i == size - 1 {
				neuronsCount = outputNeurons
				
			} else {
				neuronsCount = neuronsPerHiddenLayers
			}
		}

		n.Layers[i].Size = neuronsCount
		
		if NEURAL_DEBUG {
			log.WithFields(log.Fields{
				"Size":n.Layers[i].Size,
				"previousNeuronCount":previousNeuronCount,
			}).Info(fmt.Sprintf("Initiating layer %d", i))
		}
		
		for neuronIndex := 0; neuronIndex < neuronsCount; neuronIndex++ {
			newNeuron := Neuron{}
			newNeuron.init(previousNeuronCount)
			n.Layers[i].Neurons = append(n.Layers[i].Neurons, newNeuron)
		}
	}
}

func (n *Network) FeedForward(inputs []float64) []float64 {
	//Set values in inputLayer
	for i,v := range inputs {
		n.Layers[0].Neurons[i].Value = v
	}

	//feed to next Layers
	for layerIndex:=1; layerIndex < n.Size; layerIndex++ {
		for neuronIndex:=0; neuronIndex < n.Layers[layerIndex].Size; neuronIndex++ {
			var newNeuronValue float64 = 0
			for previousLayerNeuronIndex:=0; previousLayerNeuronIndex < n.Layers[layerIndex-1].Size; previousLayerNeuronIndex++ {
				newNeuronValue += n.Layers[layerIndex-1].Neurons[previousLayerNeuronIndex].Value * n.Layers[layerIndex].Neurons[neuronIndex].Weights[previousLayerNeuronIndex]
			}
			//add bias and pass to activation function
			newNeuronValue += n.Layers[layerIndex].Neurons[neuronIndex].Bias*n.Layers[layerIndex].Neurons[neuronIndex].BiasWeight
			n.Layers[layerIndex].Neurons[neuronIndex].Value = n.Layers[layerIndex].Neurons[neuronIndex].ActivationFunction(newNeuronValue)
		}
	}

	outputLayer := n.Layers[n.Size-1:][0]
	outputLayerValues := make([]float64, outputLayer.Size)
	for i, outputNeuron := range outputLayer.Neurons {
		outputLayerValues[i] = outputNeuron.ActivationFunction(outputNeuron.Value)
	}

	return outputLayerValues
}

func (n Network) CrossBreed(n2 Network) Network{
	newNetwork := n;
	
	randSource := rand.NewSource(time.Now().UnixNano())
	randGen := rand.New(randSource)
	for layerIndex:=1; layerIndex < n.Size; layerIndex++ {
		for neuronIndex:=0; neuronIndex < n.Layers[layerIndex].Size; neuronIndex++ {

			for i,_ :=range n.Layers[layerIndex].Neurons[neuronIndex].Weights {
				if (randGen.NormFloat64() > 0.0) {
					newNetwork.Layers[layerIndex].Neurons[neuronIndex].Weights[i] = n2.Layers[layerIndex].Neurons[neuronIndex].Weights[i]
				} else {
					newNetwork.Layers[layerIndex].Neurons[neuronIndex].Weights[i] = n2.Layers[layerIndex].Neurons[neuronIndex].Weights[i]
				}
			}

			if (randGen.NormFloat64() > 0.0) {
				newNetwork.Layers[layerIndex].Neurons[neuronIndex].BiasWeight = n2.Layers[layerIndex].Neurons[neuronIndex].BiasWeight
			} else {
				newNetwork.Layers[layerIndex].Neurons[neuronIndex].BiasWeight = n.Layers[layerIndex].Neurons[neuronIndex].BiasWeight
			}
		}
	}

	return newNetwork
}

func (n *Network) Mutate() {
	for layerIndex:=1; layerIndex < n.Size; layerIndex++ {
		for neuronIndex:=0; neuronIndex < n.Layers[layerIndex].Size; neuronIndex++ {
			for i,_ :=range n.Layers[layerIndex].Neurons[neuronIndex].Weights {
				randSource := rand.NewSource(time.Now().UnixNano())
				randGen := rand.New(randSource)
				chance := randGen.Intn(1) 
				log.Println(chance)
				if (chance > 1) {
					if (randGen.Intn(1) == 0) {
						n.Layers[layerIndex].Neurons[neuronIndex].Weights[i] *= 0.99//randGen.NormFloat64() 
					} else {
						n.Layers[layerIndex].Neurons[neuronIndex].Weights[i] *= 1.01
					}
				} else {
					n.Layers[layerIndex].Neurons[neuronIndex].Weights[i] = randGen.NormFloat64() 
				}
			}
		}
	}
}
