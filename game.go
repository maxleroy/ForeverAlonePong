package main

import "log"

type Game struct {
	Id int
	Brain Network
	State string
	Score int
	BallX int
	BallY int
	RacketTop int
	RacketBottom int
}

func (g *Game) update(m RequestMessage) {
	g.Score = m.Score
	g.State = m.GameState
	g.BallX = m.Ball.X
	g.BallY = m.Ball.Y
	g.RacketTop = m.Racket.Top
	g.RacketBottom = m.Racket.Bottom
}

func (g Game) getDirection() string {
	inputs := []float64{
		float64(g.BallY),
		float64(g.RacketTop+50), //top + half of it's height
	}

	output := g.Brain.FeedForward(inputs)
	var up bool

	log.Println("output : " , output, "input : " , inputs)

	//up = (output[0] > output[1])
	up = (output[0] > 0.5)
	if (up) {
		return "up" 
	} else {
		return "down"
	}
}