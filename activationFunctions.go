package main

import "math"

type activationFunction func(float64) float64

func SigmoidalTransfer(d float64) float64 {
	return 1 / (1 + math.Pow(math.E, - d))
}

func SigmoidalTransferDerivate(d float64) float64 {
	return 1.0
}

func Relu(x float64) float64 {
	return math.Max(0, 0.5*x)
}

